package com.inneractive.logs;

import org.apache.commons.io.FileUtils;
import org.apache.commons.lang3.StringUtils;

import java.io.File;
import java.io.IOException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class LogParserTest {
    public static void main(String[] args) throws IOException {
        String s = "endpoint,aid,dynamicfp,refURL,bid,v,ua,cip,f,nt,l,lg,aaid,g,fs,refurl,a,asha,amd,iesha,iemd,dnt,appv,dml,ciso,mcc,mnc,t,crn,w,h,o,fromSDK,idfa,med,isdks,po,ow,oh,ismd,iA-Host,cid,orientation,appName,rw,rh,hid,,k,osudi,test,time,iabCategories,mw,timestamp,mmd,msha,issha,aaID,cuid,c,dmd,dsha,iA-Via,iA-Accept,idfv,iA-Accept-Charset,iA-Cookie,hacc,vacc,tacc,noadstring,callback,iA-Accept-Encoding,mn,iA-X-Nokia-Gateway-Id,iA-X-Nokia-BEARER,iA-X-Nokia-MaxDownlinkBitrate,iA-X-Nokia-MaxUplinkBitrate,eid,at,iA-x-up-calling-line-id,iA-x-source-id,iA-x-up-bear-type,iA-x-wap-profile,iA-x-forward-for,iA-X-UIDH,iA-X-BlueCoat-Via,iA-X-Forwarded-For,X-IA-Pricing,X-IA-Pricing-Value,X-IA-Pricing-Currency,X-IA-Ad-Width,X-IA-Ad-Height,X-IA-Cid,X-IA-Session,X-IA-Error,X-IA-AdNetwork,X-IA-Ad-Type";
        List<String> lines = FileUtils.readLines(new File("/home/lior/Desktop/after"));
        int baseValue = 0;
        int numberOfMismatchedLines = 0;
        for (String line : lines) {
            int i = StringUtils.countMatches(line, ",");
            if (lines.indexOf(line) == 0) {
                baseValue = i;
                System.out.println(baseValue);
            } else {
                if (i != baseValue) {
                    System.out.println(line);
                    numberOfMismatchedLines++;
                }
            }
        }
        System.out.println(numberOfMismatchedLines);
        //LogParser.main(new String[]{"/home/lior/Desktop/before", "/home/lior/Desktop/after"});
    }
}
