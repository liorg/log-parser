package com.inneractive.logs;

import org.apache.commons.lang3.StringUtils;

import java.io.*;
import java.util.*;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * User: lior
 * Date: 12/24/13
 */
public class LogParser {

    private static final Pattern linePattern = Pattern.compile("HttpRequest\\(GET,(.*),List\\((.*)\\),.*Response-Headers: (.*) sc");
    private static final Pattern urlPattern = Pattern.compile(".*/simpleM2M/(.*)");
    private static final String ENDPOINT = "endpoint";
    private static Set<String> urlParameters = new LinkedHashSet<>();
    private static Set<String> responseHeaders = new LinkedHashSet<>();

    public static void main(String[] args) throws IOException {
        File inputFile = new File(args[0]);
        File outputFile = new File(args[1]);
        urlParameters.add(ENDPOINT);
        BufferedReader bufferedReader = new BufferedReader(new FileReader(inputFile));
        BufferedWriter bufferedWriter = new BufferedWriter(new FileWriter(outputFile));
        String line;
        System.out.println("Started parameters reads");
        while ((line = bufferedReader.readLine()) != null) {
            handleLineParameters(line);
        }
        bufferedWriter.write(StringUtils.join(urlParameters.iterator(), ",") + "," + StringUtils.join(responseHeaders.iterator(), ","));
        bufferedReader = new BufferedReader(new FileReader(inputFile));
        System.out.println("Started value reads");
        while ((line = bufferedReader.readLine()) != null) {
            Matcher matcher = linePattern.matcher(line);
            if (matcher.find())
                bufferedWriter.write(readLineParameters(matcher.group(1)) + "," + readLineResponseHeaders(matcher.group(3)) + "\n");
        }
        bufferedReader.close();
        bufferedWriter.flush();
        bufferedWriter.close();
    }

    private static String readLineResponseHeaders(String requestHeaders) {
        String[] headers = requestHeaders.split("X-IA");
        Map<String, String> lineHeadersMap = new HashMap<>();
        List<String> lineResult = new ArrayList<>();
        for (String header : headers) {
            if (!"".equals(header)) {
                String[] parts = header.split("=");
                lineHeadersMap.put("X-IA" + parts[0], parts[1].trim());
            }
        }
        for (String responseHeader : responseHeaders) {
            String value = lineHeadersMap.get(responseHeader);
            lineResult.add(value != null ? value : "");
        }
        return StringUtils.join(lineResult.iterator(), ",");
    }

    private static String readLineParameters(String url) {
        Matcher matcher = urlPattern.matcher(url);
        List<String> lineResult = new ArrayList<>();
        if (matcher.find()) {
            String[] endpointAndParameters = matcher.group(1).split("\\?");
            String endpoint = endpointAndParameters[0];
            String[] parameters = endpointAndParameters[1].split("&");
            Map<String, String> lineParametersMap = new HashMap<>();
            for (String parameter : parameters) {
                String[] parts = parameter.split("=");
                String value = parts.length > 1 ? parts[1] : "";
                lineParametersMap.put(parts[0], value);
            }
            for (String urlParameter : urlParameters) {
                String value;
                if (ENDPOINT.equals(urlParameter)) {
                    value = endpoint;
                } else {
                    value = lineParametersMap.get(urlParameter);
                }
                lineResult.add(value != null ? value.replace(",", "%2C") : "");
            }
        }
        return StringUtils.join(lineResult.iterator(), ",");
    }

    private static void handleLineParameters(String line) {
        Matcher matcher = linePattern.matcher(line);
        if (matcher.find()) {
            addUrlParameters(matcher.group(1));
            addResponseHeaders(matcher.group(3));
        }
    }

    private static void addResponseHeaders(String requestHeaders) {
        String[] headers = requestHeaders.split("X-IA");
        for (String header : headers) {
            if (!"".equals(header)) {
                responseHeaders.add("X-IA" + header.split("=")[0]);
            }
        }
    }

    private static void addUrlParameters(String url) {
        Matcher matcher = urlPattern.matcher(url);
        if (matcher.find()) {
            String[] parameters = matcher.group(1).split("\\?")[1].split("&");
            for (String parameter : parameters) {
                urlParameters.add(parameter.split("=")[0]);
            }
        }
    }
}
